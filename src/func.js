const getSum = (str1, str2) => {
  // add your implementation below
  if (isNaN(str1) || isNaN(str2) || typeof str1 !== 'string' || typeof str2 !== 'string') {
    return !this
  }
  let str1Arr
  let str2Arr
  if (str1.length === 0) {
    str1Arr = [0]
  } else {
    str1Arr = str1.split('').map(Number)
  }
  if (str2.length === 0) {
    str2Arr = [0]
  } else {
    str2Arr = str2.split('').map(Number)
  }
  // Sum arrays
  let c = [];
  for (let i = 0; i < Math.max(str1Arr.length, str2Arr.length); i++) {
    c.push((str1Arr[i] || 0) + (str2Arr[i] || 0));
  }


  return c.join('')
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
  let sumComments = 0
  let sumPosts = 0

  // if object has a value 
  listOfPosts.forEach((post) => {
    // Check if object has nested array

    if (post.comments !== undefined) {
      post.comments.forEach((comment) => {
        if (Object.values(comment).indexOf(authorName) > -1) {
          sumComments += 1
        }
      })
    }
    // check if author exist in post 
    if (Object.values(post).indexOf(authorName) > -1) {
      sumPosts += 1
    }
  })

  return `Post:${sumPosts},comments:${sumComments}`;
};

const tickets = (people) => {
  // add your implementation below
  const ticketPrice = 25
  let sum = 0
  let changeToPay = 0
  let resStr = ''

  people.forEach((person) => {
    if (+person > ticketPrice) {

      changeToPay = person - ticketPrice

      if (sum - changeToPay >= 0) {
        console.log('yes')
        sum += (+person) - changeToPay
        console.log(sum)
      } else {
        console.log('no')
        resStr = 'NO'
      }
    } else {
      sum += +person
    }
  })

  if (resStr !== 'NO') {
    resStr = "YES"
  }

  return resStr
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
